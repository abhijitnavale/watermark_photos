# WaterMark Photos
## Process Multiple Photos for Blog

A bash shell script, to  
  1. Remove EXIF Personal Information  
  3. Apply Watermark Image    
to all photos in a folder and save in new directory.

# PRECAUTION
**Do not run the script directly on original photos.**  
Create copy of original photos in some other folder.

# Warning
Running the script again on same photo folder will overwrite the `wartermarked` directory contents.

# ImageMagick.  
## Install in Ubuntu
`sudo apt-get update`  
`sudo apt-get install imagemagick`

# How to Use  
  1. Install ExifTool  
  2. Install ImageMagick  
  3. Place your watermark file in your hame folder and rename it `watermark.png`.  
  4. Download the script in your home folder.  
  4. Replace `<user>` in this script, with your Gnu/Linux user name.  
  5. Modify other details in this script such as scaling `width x height`  
  6. `cd ~`  
  7. `chmod +x watermark.sh`  
  8. `./watermark.sh /home/user/<path_to_directory_with_copy_of_original_photos> <logo_position>`

Type just the command `./watermark.sh` without any arguments will list available logo gravity positions along with error.  

`<logo_position>` can be `NorthWest, North, NorthEast, West, Center, East, SouthWest, South or SouthEast`  
Read more about gravity option at https://www.imagemagick.org/script/command-line-options.php#gravity

After successful processing, you can find Final Watermark applied photos at,  
`<path_to_directory_with_copy_of_original_photos>/watermarked`

# Credits

Photo Videos Icon by Baboon Designs from [The Noun Project](https://thenounproject.com)

# License

## MIT License

Copyright (c) 2017 Abhijit Navale

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

